#!/usr/bin/env bash

for i in {1..4}
do
  for j in {1..5}
  do
    DIR=${i}/${j}
    FILE=${DIR}/file.txt
    mkdir -p ${DIR}
    touch ${FILE}
    touch -d "$(date -R -r ${FILE}) - ${i} hours" ${FILE}
  done
done
